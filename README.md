# mqr

#### 介绍
茉莉QQ机器人，采用Mirai的android协议实现的QQ机器人服务。

#### 软件架构
使用java的springboot框架开发，运行软件环境需要jdk8 +

#### 在线文档
[访问在线文档，与程序同步更新](http://mqr.molicloud.com)